<?php

namespace Finpaa\Sweden;

use Finpaa\Finpaa;

class Lansforsakringar
{
    private static $products;
    private static $ACCESS_TOKEN;

    const DOMESTIC_CREDIT_TRANSFER = 'domestic-credit-transfers';
    const DOMESTIC_GIRO = 'domestic-giros';
    const CROSS_BORDER_CREDIT_TRANSFER = 'cross-border-credit-transfers';

    private static function selfConstruct()
    {
        $finpaa = new Finpaa();
        self::$products = $finpaa->Sweden()->{env('FINPAA_ENVIRONMENT')}()->Lansforsakringar()->Products();
    }

    private static function getSequenceCode($name = 'products')
    {
        if(self::$$name) {
            return self::$$name;
        }
        else {
            self::selfConstruct();
            return self::getSequenceCode($name);
        }
    }

    private static function executeMethod($methodCode, $alterations, $returnPayload = false)
    {
        $response = Finpaa::executeTheMethod($methodCode, $alterations, $returnPayload);
        return array('error' => false, 'response' => json_decode(json_encode($response), true));
    }

    static function redirectAuthorize()
    {
        $methodCode = self::getSequenceCode()->token()->oauth2Token();

        $alterations[]['body'] = array(
            'client_id' => env('LANS_CLIENT_ID'),
            'client_secret' => env('LANS_CLIENT_SECRET'),
            'grant_type' => 'client_credentials'
        );

        $response = self::executeMethod($methodCode, $alterations);

        return $response;
    }

    static function getAccounts($alterations)
    {
        $methodCode = self::getSequenceCode()->privateAccount()->accountsRWithBalanceTrue();

        $response = self::executeMethod($methodCode, $alterations);

        return $response;
    }

    static function getTransactions($alterations)
    {
        $methodCode = self::getSequenceCode()->privateAccount()->accountsRTransactionsBothStartpage();

        $response = self::executeMethod($methodCode, $alterations);

        return $response;
    }

    static function initiatePayment($paymentType, $alterations)
    {
        if($paymentType === self::DOMESTIC_CREDIT_TRANSFER)
            $methodCode = self::getSequenceCode()->pispV4Create()->domesticCreditTransfers();
        else if($paymentType === self::DOMESTIC_GIRO)
            $methodCode = self::getSequenceCode()->pispV4Create()->domesticGiros();
        else if($paymentType === self::CROSS_BORDER_CREDIT_TRANSFER)
            $methodCode = self::getSequenceCode()->pispV4Create()->crossBorderCreditTransfers();

        $response = self::executeMethod($methodCode, $alterations);

        return $response;
    }
}
